require 'socket'

class Scan
  attr_writer :host  , :port
  attr_reader :port
  def initialize
    puts "Welcome to port_scaner :)"
  end
  
  def self.info(host , start_port , end_port)
    until start_port == end_port
      self.open_port(host: host , port: start_port)
      start_port += 1
    end
  end


  private 
  def self.open_port(host: , port:)
    @host = host
    @port = port
    sock = Socket.new(:INET , :STREAM)
    raw = Socket.sockaddr_in(@port , @host)
    puts "#{@port} open." if sock.connect(raw)

   rescue (Errno::ECONNREFUSED)
    rescue(Errno::ETIMEDOUT)
    rescue(Errno::EMFILE)
  end


  end

unless ARGV[0] && ARGV[1] && ARGV[2]
  puts
  warning = <<~ERR
    [:1] Please enter hostname in argument/0
    [:2] Please enter start_port in argument/1
    [:3] Please enter end_port in argument/2
    ERR

    puts warning
end




